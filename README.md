# README #

This is a sample project created to get help on a specific issues in a real project.

### What is this repository for? ###

* Getting some help in solving the problem posted on [StackOverflow](http://stackoverflow.com/questions/30823544/ios-segue-freeze-for-many-seconds-before-showing-new-view)
* The project has been created with Xcode 6.3.2