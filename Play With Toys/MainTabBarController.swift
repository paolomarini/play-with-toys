//
//  MainTabBarController.swift
//  Play With Toys
//
//  Created by Paolo Marini on 24/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate, NeedsBotsRepository
{
    var _botsRepository: BotsRepository?
    
    var botsRepository: BotsRepository? // injected by the AppDelegate
    {
        get
        {
            return _botsRepository
        }
        
        set(newRepository)
        {
            _botsRepository = newRepository
            
            if let vcs = self.viewControllers as? [UIViewController]
            {
                for viewController in vcs
                {
                    var vc = viewController as? NeedsBotsRepository
                    
                    if vc != nil
                    {
                        vc!.botsRepository = newRepository
                    }

                }
            }
        }
    }

}
