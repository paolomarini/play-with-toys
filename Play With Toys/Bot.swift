//
//  Bot.swift
//  Play With Toys
//
//  Created by Paolo Marini on 24/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

public class Bot
{

    var Id: String = ""
    var Name: String = ""
    var Filename: String = ""
    var Description: String = ""
    var Purchased: Bool = false
    var Price: String = ""
    
    public init() { }
       
}