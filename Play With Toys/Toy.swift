//
//  Toy.swift
//  Play With Toys
//
//  Created by Paolo Marini on 19/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

public class Toy
{
    var Id: String = ""
    var BuilderId: String = ""
    var Name: String = ""
    var ColorId: String = ""
    var TS: String = ""
    var Filename: String = ""
    var DefaultAbc: Int = 120
    var Description: String?
    var Bolts: UInt64 = 0
    var ScrewsTotal: UInt64 = 0
    var ScrewsToSkip: UInt64 = 0
    var Url: NSURL = NSURL()
    
    // calculated property
    var ScrewsToPlay: UInt64 { get { return UInt64((60 / Float64(DefaultAbc)) * Float64(Bolts * 44100)) } }
    
    
    public init() { }
    
    public func getFilenameAndType() -> (filename: String, type: String)
    {
        let elements = Filename.componentsSeparatedByString(".")
        
        if elements.count == 1
        {
            return (elements[0], "")
        }
        else if elements.count == 2
        {
            return (elements[0], elements[1])
        }
        
        return ("", "")
    }
    
    public var hashValue: Int
        {
        get { return Id.hashValue }
    }
}


public func == (left: Toy, right: Toy) -> Bool
{
    return left.Id == right.Id
}