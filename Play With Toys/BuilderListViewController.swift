//
//  BuilderListViewController
//  Play With Toys
//
//  Created by Paolo Marini on 19/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import UIKit

class BuilderListViewController: UIViewController, UITableViewDataSource, NeedsBotsRepository
{
    
    @IBOutlet weak var buildersTable: UITableView!
    
    var builders = Dictionary<String, Builder>()
    var builderIds = [String]()
    
    var botsRepository: BotsRepository?  // injected by the AppDelegate (via the MainTabBarController setter)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        builders = botsRepository!.builders
        builderIds = Array(builders.keys)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Implementation of UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return builders.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let builder = builders[builderIds[indexPath.row]]
        
        // alternating alignment left/right
        let left = (indexPath.row % 2) == 0
       
        var cell:UITableViewCell
        
        if left
        {
            cell = tableView.dequeueReusableCellWithIdentifier("BuilderCellLeft") as! UITableViewCell
            var labelView = cell.viewWithTag(2) as! UILabel
            labelView.text = "      \(builder!.Name)    " // quite ugly but it works for now
        }
        else
        {
            cell = tableView.dequeueReusableCellWithIdentifier("BuilderCellRight") as! UITableViewCell
            var labelView = cell.viewWithTag(2) as! UILabel
            labelView.text = "    \(builder!.Name)     ." // quite ugly but it works for now
        }

        // lots of trouble with the line below
        // if it returns nil, try Unchecking Size classes in the storyboard, clean & build and recheck Size classes option
        // http://stackoverflow.com/a/28810514/1005442
        var imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + builder!.Filename)!
        
        return cell
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        var builderIndex = buildersTable.indexPathForSelectedRow()?.row

        if let destination = segue.destinationViewController as? BuilderToysListViewController
        {
            destination.botsRepository = botsRepository!
            destination.builder = builders[builderIds[builderIndex!]]
        }

    }

}