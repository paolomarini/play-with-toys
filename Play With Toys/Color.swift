//
//  Color.swift
//  Play With Toys
//
//  Created by Paolo Marini on 31/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

public class Color: Equatable, Hashable
{
    var Id: String = ""
    var Name: String = ""
    var Filename: String = ""
    var FilenameSelected: String = ""
    
    public var hashValue: Int
    {
        get { return Id.hashValue }
    }
}


public func == (left: Color, right: Color) -> Bool
{
    return left.Id == right.Id
}