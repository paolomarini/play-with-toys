//
//  Builder.swift
//  Play With Toys
//
//  Created by Paolo Marini on 19/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

public class Builder: Equatable, Hashable
{
    var Id: String = ""
    var Name: String = ""
    var Filename: String = ""
    var Url: NSURL = NSURL()
    
    public init() { }
    
    public func getFilenameAndType() -> (filename: String, type: String)
    {
        let elements = Filename.componentsSeparatedByString(".")
        
        if elements.count == 1
        {
            return (elements[0], "")
        }
        else if elements.count == 2
        {
            return (elements[0], elements[1])
        }
        
        return ("", "")
    }
    
    public var hashValue: Int
        {
            get
            {
                return Id.hashValue
            }
        }
}

public func == (left: Builder, right: Builder) -> Bool
{
    return left.Id == right.Id
}
