//
//  SecondViewController.swift
//  Play With Toys
//
//  Created by Paolo Marini on 19/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import UIKit

class BuilderToysListViewController: UIViewController, UICollectionViewDataSource, UITableViewDataSource, UICollectionViewDelegate, NeedsBotsRepository
{

    @IBOutlet weak var builderImageView: UIImageView!
    @IBOutlet weak var builderLabel: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var toysTable: UITableView!
    
    private let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    private let timeSignaturesCollectionViewTag = 1
    private let colorsCollectionViewTag = 2
    private let selectedTSBackgroundColor = UIColor(red:0.33, green:0.63, blue:0.99, alpha:1.0)
    private let unselectedTSBackgroundColor = UIColor.lightGrayColor()
    private let selectedTSBorderColor = UIColor.whiteColor().CGColor
    private let unselectedTSBorderColor = UIColor(red:0.59, green:0.59, blue:0.59, alpha:1.0).CGColor

    var botsRepository: BotsRepository?  // injected by the segue in BuilderListViewController
    var builder: Builder? // injected by the segue in BuilderListViewController
    var toys: [Toy] = []
    var timeSignatures: [String] = []
    var colors: [Color] = []
    
    var selectedTSs = Dictionary<String, String>()
    var selectedColors = Dictionary<String, Color>()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        timeSignatures = botsRepository!.getTSsByBuilder(builder!)
        colors = botsRepository!.getColorsByBuilder(builder!)
        toys = botsRepository!.getToysWithCriteria(builder!)

        navigationBar.topItem?.title = "Toys by \(builder!.Name)"
        builderImageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + builder!.Filename)!
        builderLabel.text = builder!.Name
        
        applySelectionDefaults()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func applySelectionDefaults()
    {
        if selectedTSs.isEmpty
        {
            for timeSignature in timeSignatures
            {
                selectedTSs[timeSignature] = timeSignature
            }
        }
        
        if selectedColors.isEmpty
        {
            for color in colors
            {
                selectedColors[color.Id] = color
            }
        }
    }
    
    func updateToys()
    {
        toys = botsRepository!.getToysWithCriteria(builder!, timeSignatures: selectedTSs.keys.array, colorIds: selectedColors.keys.array)
        
        toysTable.reloadData()
    }
    
    func updateTSCellColor(cell: UICollectionViewCell)
    {
        var label = cell.viewWithTag(1) as! UILabel
        
        if cell.selected
        {
            label.backgroundColor = selectedTSBackgroundColor
            label.layer.borderColor = selectedTSBorderColor
        }
        else
        {
            label.backgroundColor = unselectedTSBackgroundColor
            label.layer.borderColor = unselectedTSBorderColor
        }
    }
    
    // MARK: - Implementation of UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == timeSignaturesCollectionViewTag
        {
            return timeSignatures.count
        }
        else if collectionView.tag == colorsCollectionViewTag
        {
            return colors.count
        }
        
        return 0
    }
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        if collectionView.tag == timeSignaturesCollectionViewTag
        {
            var cell:UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("TS", forIndexPath: indexPath) as! UICollectionViewCell
            
            var timeSignature = timeSignatures[indexPath.row]
            
            var label = cell.viewWithTag(1) as! UILabel
            label.text = timeSignature
            label.layer.borderWidth = 1
            
            cell.selected = selectedTSs[timeSignature] != nil
            collectionView.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)

            updateTSCellColor(cell)
            
            return cell
        }
        else if collectionView.tag == colorsCollectionViewTag
        {
            var cell:UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Color", forIndexPath: indexPath) as! UICollectionViewCell
            
            let color = colors[indexPath.row]

            cell.selected = selectedColors[color.Id] != nil
            collectionView.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
            
            var imageView = cell.viewWithTag(2) as! UIImageView
            imageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + (cell.selected ? color.FilenameSelected : color.Filename))!

            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return sectionInsets
    }

    
    // MARK: - Implementation of UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var cell = collectionView.cellForItemAtIndexPath(indexPath)!

        if collectionView.tag == timeSignaturesCollectionViewTag
        {
            let timeSignature = timeSignatures[indexPath.row]
            selectedTSs[timeSignature] = timeSignature
            
            updateTSCellColor(cell)
        }
        else if collectionView.tag == colorsCollectionViewTag
        {
            let color = colors[indexPath.row]
            selectedColors[color.Id] = color
            
            var imageView = cell.viewWithTag(2) as! UIImageView
            imageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + color.FilenameSelected)!
        }
        
        updateToys()
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath)
    {
        var cell = collectionView.cellForItemAtIndexPath(indexPath)!

        if collectionView.tag == timeSignaturesCollectionViewTag
        {
            let timeSignature = timeSignatures[indexPath.row]
            selectedTSs.removeValueForKey(timeSignature)
            
            updateTSCellColor(cell)
        }
        else if collectionView.tag == colorsCollectionViewTag
        {
            let color = colors[indexPath.row]
            selectedColors.removeValueForKey(color.Id)
            
            var imageView = cell.viewWithTag(2) as! UIImageView
            imageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + color.Filename)!
        }
        
        updateToys()
    }
    
    
    // MARK: - Implementation of UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return toys.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let toy = toys[indexPath.row]
        let color = botsRepository!.colors[toy.ColorId]!
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("ToyCell") as! UITableViewCell
        
        // lots of trouble with the line below
        // if it returns nil, try Unchecking Size classes in the storyboard, clean & build and recheck Size classes option
        // http://stackoverflow.com/a/28810514/1005442
        var imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath! + "/Bots/" + color.FilenameSelected)!
        
        var name = cell.viewWithTag(2) as! UILabel
        name.text = toy.Name
        
        var description = cell.viewWithTag(3) as! UILabel
        description.text = toy.Description
        //description.backgroundColor = UIColor.orangeColor()
        description.sizeToFit()
        
        var timeSignature = cell.viewWithTag(4) as! UILabel
        timeSignature.text = toy.TS
        
        return cell
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.        
        
        var controller = (segue.destinationViewController as! BuilderListViewController)
        controller.botsRepository = botsRepository!
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        performSegueWithIdentifier("segueToysByBuilder", sender: nil)
    }
    
    
}