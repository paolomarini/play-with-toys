//
//  BotsRepository.swift
//  Play With Toys
//
//  Created by Paolo Marini on 31/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

class BotsRepository: NSObject, NSXMLParserDelegate
{
    var bots = Dictionary<String, Bot>()
    var builders = Dictionary<String, Builder>()
    var colors = Dictionary<String, Color>()
    var toys = Dictionary<String, Toy>()
    var botFiles: [String] = []
    
    private var currentFile: String = ""
    private var currentBot:Bot? = nil
    private var currentBuilder:Builder? = nil
    private var currentColor:Color? = nil
    private var currentToy:Toy? = nil
    private var builderSet = Set<Builder>() // used to avoid duplicates
    
       
    override init()
    {
        super.init()
        
        loadBots()
        
        for builder in builderSet
        {
            builders[builder.Id] = builder
        }

    }
    
/*    func getToysByBuilder(builder: Builder) -> [Toy]
    {
        var result = [Toy]()
        
        for toy in toys.values
        {
            if (toy.BuilderId == builder.Id)
            {
                result.append(toy)
            }
        }
        
        return result
    }
*/  

    func getToysWithCriteria(builder: Builder, timeSignatures: [String]? = nil, colorIds: [String]? = nil) -> [Toy]
    {
        var result = [Toy]()
        
        for toy in toys.values
        {
            if (toy.BuilderId == builder.Id)
                && (timeSignatures == nil || timeSignatures!.isEmpty || contains(timeSignatures!, toy.TS))
                && (colorIds == nil || colorIds!.isEmpty || contains(colorIds!, toy.ColorId))
            {
                result.append(toy)
            }
        }
        
        return result
    }
    
    func getTSsByBuilder(builder: Builder) -> [String]
    {
        var temp_result = [String]()
        
        for toy in toys.values
        {
            if toy.BuilderId == builder.Id
            {
                temp_result.append(toy.TS)
            }
        }
        
        var unique = Array(Set(temp_result))
        
        // need to sort it
        var t = sorted(unique, { t1, t2 in self.timeSignatureComparison(t1, time2: t2) } )
        
        return t
    }
    
    func getColorsByBuilder(builder: Builder) -> [Color]
    {
        var temp_result = [Color]()
        
        for toy in toys.values
        {
            if toy.BuilderId == builder.Id
            {
                temp_result.append(colors[toy.ColorId]!)
            }
        }
        
        var unique = Array(Set(temp_result))
        
        // need to sort it
        var s = sorted(unique, { s1, s2 in return s1.Name < s2.Name } )
        
        return s       
    }
    
    func timeSignatureComparison(time1: String, time2: String) -> Bool
    {
        var timeArr1 = split(time1) {$0 == "/"}
        var timeArr2 = split(time2) {$0 == "/"}
        
        // TODO: optimise
        return timeArr1[1].toInt() < timeArr2[1].toInt() || (timeArr1[1].toInt() == timeArr2[1].toInt() && timeArr1[0].toInt() < timeArr2[0].toInt())
    }

    
    private func getBotFiles() -> [String]
    {
        var botFiles:[String] = []
        
        let fileManager = NSFileManager.defaultManager()
        let botsPath = NSBundle.mainBundle().resourcePath! + "/Bots"
        
        var error: NSError?
        let filenames = fileManager.contentsOfDirectoryAtPath(botsPath, error:&error) as! [String]
        
        // TODO: check for errors
        
        let botFilenames = filenames.filter{ (file) in file.endsWith(".xml") }
        
        for botFilename in botFilenames
        {
            botFiles.append(botsPath + "/" + botFilename)
            //            println(botsPath + "/" + botFilename)
        }
        
        return botFiles
    }

    private func loadBots()
    {
        botFiles = getBotFiles()
        
        bots.removeAll(keepCapacity: true)
        
        for botFile in botFiles
        {
            currentFile = botFile
            addBotsFromFile(botFile)
        }
    }
    
    private func addBotsFromFile(filePath: String)
    {
        var data:NSData = NSData(contentsOfFile: filePath)!
        
        var parser = NSXMLParser(data: data)
        parser.delegate = self
        
        var success:Bool = parser.parse()
        
        if !success
        {
            println("Bots parsing failure")
        }
    }
    
    // MARK: - Implementation of NSXMLParserDelegate
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject])
    {
        
        if elementName == "bot"
        {
            currentBot = Bot()
            
            // mandatory fields
            currentBot!.Id = attributeDict["id"] as! String
            currentBot!.Name = attributeDict["name"] as! String
            currentBot!.Filename = currentFile
            currentBot!.Description = attributeDict["description"] as! String
            
            // optional fields
            currentBot!.Price = attributeDict["price"] as? String ?? ""
            
            let purchased = attributeDict["purchased"] as? String ?? "false"
            currentBot!.Purchased = NSString(string: purchased).boolValue
        }
        else if elementName == "builder"
        {
            currentBuilder = Builder()
            
            // mandatory fields
            currentBuilder!.Id = attributeDict["id"] as! String
            currentBuilder!.Name = attributeDict["name"] as! String
            currentBuilder!.Filename = attributeDict["filename"] as! String
        }
        else if elementName == "toy" {
            currentToy = Toy()
            
            // mandatory fields
            currentToy!.Id = attributeDict["id"] as! String
            currentToy!.BuilderId = attributeDict["builder-id"] as! String
            currentToy!.Name = attributeDict["name"] as! String
            currentToy!.ColorId = attributeDict["color-id"] as! String
            currentToy!.TS = attributeDict["t-s"] as! String
            currentToy!.Filename = attributeDict["filename"] as! String
            currentToy!.DefaultAbc = (attributeDict["default-abc"] as! String).toInt()!
            currentToy!.ScrewsToSkip = UInt64((attributeDict["screws-to-skip"] as! NSString).longLongValue)
            currentToy!.Bolts = UInt64((attributeDict["bolts"] as! NSString).longLongValue)
            
            // optional fields
            currentToy!.Description = attributeDict["description"] as? String
            
            // calculated fields
            let filePath = NSBundle.mainBundle().resourcePath! + "/Bots/\(currentToy!.Filename)"
            currentToy!.Url = NSURL(fileURLWithPath: filePath)!
        }
        else if elementName == "color"
        {
            currentColor = Color()
            
            // mandatory fields
            currentColor!.Id = attributeDict["id"] as! String
            currentColor!.Name = attributeDict["name"] as! String
            currentColor!.Filename = attributeDict["filename"] as! String
            currentColor!.FilenameSelected = attributeDict["filename-selected"] as! String
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if currentBot != nil && elementName == "bot"
        {
            bots[currentBot!.Id] = currentBot!
            currentBot == nil
        }
        else if currentBuilder != nil && elementName == "builder"
        {
            builderSet.insert(currentBuilder!)
            currentBuilder == nil
        }
        else if currentToy != nil && elementName == "toy"
        {
            toys[currentToy!.Id] = currentToy!
            currentToy == nil
        }
        else if currentColor != nil && elementName == "color"
        {
            colors[currentColor!.Id] = currentColor!
            currentColor == nil
        }

    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?)
    {
        //println("Found characters: " + string)
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError)
    {
        println("Parser error:")
        println(parseError)
    }

}