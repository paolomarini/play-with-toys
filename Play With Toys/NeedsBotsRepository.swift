//
//  NeedsBotRepositoryProtocol.swift
//  Play With Toys
//
//  Created by Paolo Marini on 24/05/2015.
//  Copyright (c) 2015 Paolo Marini. All rights reserved.
//

import Foundation

protocol NeedsBotsRepository
{
    
    var botsRepository: BotsRepository? { get set } // injected by the AppDelegate

}